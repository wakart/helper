<?php

    namespace Wakart\PHPUnit;

    use Exception;
    use PHPUnit\Framework\TestCase;
    use ReflectionClass;
    use ReflectionFunction;
    use ReflectionMethod;

    class Helper extends TestCase {

        /**
         * @throws \PHPUnit\Framework\AssertionFailedError
         */
        public static function generalTestConstructor(object|string $className, int $countParameters, int $countRequiredParameters) {
            self::generalTestMethod($className, '__construct', $countParameters, $countRequiredParameters);
        }
        /**
         * @throws \PHPUnit\Framework\AssertionFailedError
         */
        public static function generalTestFunction(string $functionName, int $countParameters, int $countRequiredParameters, string $returnType) {
            try {
                self::assertTrue(function_exists($functionName), 'Function don\'t exist');

                $f = new ReflectionFunction($functionName);
                self::testArguments($f, $countParameters, $countRequiredParameters);

                self::testReturn($f, $returnType);
            } catch (Exception $e) {
                self::fail($e->getMessage());
            }
        }

        /**
         * @throws \PHPUnit\Framework\AssertionFailedError
         */
        public static function generalTestFluentSetter(object|string $className, string $methodName) {
            self::generalTestSetter($className, $methodName, get_class($className));
        }

        /**
         * @throws \PHPUnit\Framework\AssertionFailedError
         */
        public static function generalTestSetter(object|string $className, string $methodName, string $returnType) {
            self::generalTestMethod($className, $methodName, 1, 1, $returnType);
        }

        /**
         * @throws \PHPUnit\Framework\AssertionFailedError
         */
        public static function generalTestMethod(object|string $className, string $methodName, int $countParameters, int $countRequiredParameters, ?string $returnType = null) {
            try {
                self::assertTrue(method_exists($className, $methodName), 'Method don\'t exist');

                $rc = new ReflectionClass($className);
                $method = $rc->getMethod($methodName);
                self::testArguments($method, $countParameters, $countRequiredParameters);

                self::testReturn($method, $returnType);
            } catch (Exception $e) {
                self::fail($e->getMessage());
            }
        }

        /**
         * @throws \PHPUnit\Framework\ExpectationFailedException
         */
        private static function testArguments(ReflectionMethod|ReflectionFunction $type, int $countParameters, int $countRequiredParameters) {
            self::assertSame($countParameters, $type->getNumberOfParameters(), 'The number of parameters does not match');
            self::assertSame($countRequiredParameters, $type->getNumberOfRequiredParameters(), 'The number of required parameters does not match');
        }

        /**
         * @throws \PHPUnit\Framework\ExpectationFailedException
         */
        private static function testReturn(ReflectionMethod|ReflectionFunction $type, ?string $returnType = null) {
            if ($returnType) {
                self::assertSame($returnType, $type->getReturnType()?->getName(), 'The returned type does not match');
            }
        }
    }
